package main

import (
	"gopkg.in/mgo.v2"
)

type User struct {
	UserID             string `json:"user_id" bson:"user_id"`
	SystemNotification bool   `json:"system_notification" bson:"system_notification"`
}

type Selector struct {
	UserID string `json:"user_id" bson:"user_id"`
}

type MongoDB struct {
	session *mgo.Session
}

var (
	DB_NAME         = "linebot"
	COLLECTION_NAME = "user"
)

func (mongo *MongoDB) Insert(user User) {
	mongo.session.DB(DB_NAME).C(COLLECTION_NAME).Insert(user)
}

func (mongo *MongoDB) FindAll() ([]User, error) {
	users := []User{}
	err := mongo.session.DB(DB_NAME).C(COLLECTION_NAME).Find(map[string]interface{}{}).All(&users)
	return users, err
}

func (mongo *MongoDB) UserExists(selector Selector) bool {
	count, err := mongo.session.DB(DB_NAME).C(COLLECTION_NAME).Find(selector).Count()
	if err != nil {
		panic(err)
	}
	return count > 0
}

func (mongo *MongoDB) Delete(selector Selector) {
	mongo.session.DB(DB_NAME).C(COLLECTION_NAME).Remove(selector)
}

func (mongo *MongoDB) Update(selector Selector, update User) {
	mongo.session.DB(DB_NAME).C(COLLECTION_NAME).Update(selector, update)
}
