package main

import (
	"strings"
)

var cmdPool *CmdPool

type CmdPool struct {
	cmd []string
}

func NewCmdPool() *CmdPool {
	cmdPool = &CmdPool{}
	return cmdPool
}

func (cp *CmdPool) add(text string) {
	cp.cmd = append(cp.cmd, text)
}

func (cp *CmdPool) getAndClear() string {
	cmds := strings.Join(cp.cmd, "\n")
	cp.clear()
	return cmds
}

func (cp *CmdPool) clear() {
	cp.cmd = []string{}
}
