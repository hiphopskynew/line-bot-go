package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"strings"

	"bitbucket.org/dotography-code/luciano/logger/stdout"

	"github.com/line/line-bot-sdk-go/linebot"
)

type Message struct {
	Text        string
	ReplyToken  string
	OwnerID     string
	DisplayName string
}

type DialogFlowRequest struct {
	Headers map[string]string
	Body    interface{}
}

type TextMessageCallback struct {
	ReplyToken  string
	Timestamp   int64
	UserID      string
	MessageID   string
	MessageText string
}

func buildLineBotTextMessageCallback(message TextMessageCallback) interface{} {
	m := make(map[string]interface{})
	json.Unmarshal([]byte(fmt.Sprintf(`{
		"events": [
			{
				"replyToken": "%v",
				"type": "message",
				"timestamp": %v,
				"source": {
					"type": "user",
					"userId": "%v"
				},
				"message": {
					"id": "%v",
					"type": "text",
					"text": "%v"
				}
			}
		]}`, message.ReplyToken, message.Timestamp, message.UserID, message.MessageID, message.MessageText)), &m)
	return m
}

/*
	Callback handle for LINE push
*/
func callback(w http.ResponseWriter, req *http.Request) {
	events, _ := BOT.ParseRequest(req)
	addUsers(events)

	for _, event := range events {
		switch event.Type {
		case linebot.EventTypeMessage:
			stdout.Debug("Type Message")
			switch message := event.Message.(type) {
			case *linebot.TextMessage:
				stdout.Debug("Text Message")
				profile, _ := BOT.GetProfile(event.Source.UserID).Do()
				textCommand(strings.SplitAfter(message.Text, " ")[0], Message{
					Text:        message.Text,
					ReplyToken:  event.ReplyToken,
					OwnerID:     event.Source.UserID,
					DisplayName: profile.DisplayName,
				})
			case *linebot.StickerMessage:
				stdout.Debug("Sticker Message")
				sticker := randomSticker(STICKERS)
				BOT.ReplyMessage(event.ReplyToken, linebot.NewStickerMessage(sticker.PackageID, sticker.StickerID)).Do()
			case *linebot.ImageMessage:
				BOT.ReplyMessage(event.ReplyToken, linebot.NewImageMessage(FOX_URL, FOX_URL)).Do()
			}
		case linebot.EventTypePostback:
			stdout.Debug("Type Postback")
			profile, _ := BOT.GetProfile(event.Source.UserID).Do()
			postbackCommand(strings.SplitAfter(event.Postback.Data, " ")[0], Message{
				Text:        event.Postback.Data,
				ReplyToken:  event.ReplyToken,
				OwnerID:     event.Source.UserID,
				DisplayName: profile.DisplayName,
			})
		case linebot.EventTypeUnfollow:
			stdout.Debug("Type Unfollow")
			DB.Delete(Selector{UserID: event.Source.UserID})
		}
	}
}

/*
	Push Message handle for LINE push
	JSON format example below
	{
		"user_id": "User...",
		"messages": ["Message1...", "Message2..."]
	}
*/
type PushMessageRequest struct {
	UserID   string   `json:"user_id"`
	Messages []string `json:"messages"`
}

func pushMessage(w http.ResponseWriter, r *http.Request) {
	defer r.Body.Close()
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		stdout.Debug("Push Messages Handler")
		stdout.Error(err.Error())
	}

	pm := PushMessageRequest{}
	json.Unmarshal(body, &pm)
	stdout.Debug(pm)

	msgs := removeEmptyInSlice(pm.Messages)
	if len(msgs) == 0 {
		logging(BOT.PushMessage(pm.UserID, linebot.NewTextMessage("--empty--")).Do())
		return
	}

	for _, msg := range msgs {
		logging(BOT.PushMessage(pm.UserID, linebot.NewTextMessage(msg)).Do())
	}
}

/*
	Command Message handle
	JSON format example below
	{
		"command": "/docker start phpmyadmin"
	}
*/
type CommandRequest struct {
	Command string `json:"command"`
}

func voiceCommandHandle(w http.ResponseWriter, r *http.Request) {
	defer r.Body.Close()
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		stdout.Debug("Command Messages Handler")
		stdout.Error(err.Error())
	}

	cr := CommandRequest{}
	json.Unmarshal(body, &cr)
	stdout.Debug(cr)

	// Fixed rule for command
	// Supported docker command `start` `stop` and `restart` only.
	txt := strings.Split(strings.ToLower(cr.Command), " ")
	if len(txt) != 3 {
		stdout.Debug("command more than 3 words")
		return
	}
	if txt[0] != "/docker" {
		stdout.Debug("prefix not /docker")
		return
	}
	if txt[1] != "start" && txt[1] != "stop" && txt[1] != "restart" {
		stdout.Debug("supported 'start', 'stop', and 'restart'")
		return
	}

	cmdPool.add(fmt.Sprintf("%s,docker %s %s", LINEBOT_USER_ID, txt[1], txt[2]))
	msg := fmt.Sprintf("[ระบบ]\n'%s' service %s.", txt[2], txt[1])
	logging(BOT.Multicast(getUserAllowNotiIDs(), linebot.NewTextMessage(msg)).Do())
}

/*
	ensure when using this method `text` is a command
*/
func removeCommandInText(text string) string {
	words := []string{}
	slice := strings.Split(text, " ")
	for _, v := range slice {
		if len(v) != 0 {
			words = append(words, v)
		}
	}
	return strings.Join(words[1:], " ")
}

/*
	handle command via text
*/
func textCommand(cmd string, message Message) {
	newText := removeCommandInText(message.Text)
	userIds := removeItemInSlice(getUserAllIDs(), message.OwnerID)
	trimed := strings.TrimSpace(cmd)

	switch trimed {
	case "/all":
		logging(BOT.Multicast(userIds, linebot.NewTextMessage(fmt.Sprintf("[ถึงทุกคน] จาก %s\n%s", message.DisplayName, newText))).Do())
	case "/me":
		logging(BOT.ReplyMessage(message.ReplyToken, linebot.NewTextMessage("[เฉพาะฉัน] "+newText)).Do())
	// case "/cmd":
	// 	logging(BOT.ReplyMessage(message.ReplyToken, cmdMessage()).Do())
	case "/noti":
		logging(BOT.ReplyMessage(message.ReplyToken, handleNoti(message)...).Do())
	case "/goto":
		logging(BOT.ReplyMessage(message.ReplyToken, gotoMessage()).Do())
	case "/help":
		logging(BOT.ReplyMessage(message.ReplyToken, linebot.NewTextMessage(HELP_MESSAGE)).Do())
	case "/docker":
		logging(BOT.ReplyMessage(message.ReplyToken, buildCarouselTemplate(message)).Do())
	default:
		normalMessage(trimed, message)
	}
}

/*
	build carousel for confirm command
*/
func buildCarouselTemplate(message Message) linebot.Message {
	leftBtn := linebot.NewPostbackTemplateAction("ใช่", message.Text, fmt.Sprintf("กำลังประมวลผลคำสั่ง `%v`", message.Text), "")
	rightBtn := linebot.NewMessageTemplateAction("ยังไม่ต้อง", "รับทราบ")
	template := linebot.NewConfirmTemplate(fmt.Sprintf("คุณต้องการประมวลผลคำสั่ง `%v` หรือไม่", message.Text), leftBtn, rightBtn)
	return linebot.NewTemplateMessage("การยืนยันคำสั่ง", template)
}

/*
	handle command via postback
*/
func postbackCommand(cmd string, message Message) {
	newText := removeCommandInText(message.Text)
	trimed := strings.TrimSpace(cmd)

	switch trimed {
	case "/docker":
		reserve := []string{"\\", ";", "&", "<"}
		if !contains(newText, reserve) {
			cmdPool.add(fmt.Sprintf("%s,docker %s", message.OwnerID, newText))
			return
		}

		logging(BOT.ReplyMessage(message.ReplyToken, linebot.NewTextMessage(fmt.Sprintf("ยังไม่รองรับคำสั่ง %s", reserve))).Do())
	}
}

/*
	Logging to server log when response message from LINE occur
*/
func logging(br *linebot.BasicResponse, e error) {
	if e != nil {
		stdout.Debug("Logging function")
		stdout.Error(e)
	}
}

/*
	Handle system notification
*/
func handleNoti(message Message) []linebot.Message {
	switch removeCommandInText(message.Text) {
	case "enabled":
		sticker := randomSticker(STICKER[STICKER_GROUP.SURPRISE])
		DB.Update(Selector{UserID: message.OwnerID}, User{UserID: message.OwnerID, SystemNotification: true})
		return []linebot.Message{
			linebot.NewStickerMessage(sticker.PackageID, sticker.StickerID),
			linebot.NewTextMessage("เปิดการแจ้งเตือนจากระบบแล้ว"),
		}
	case "disabled":
		sticker := randomSticker(STICKER[STICKER_GROUP.SAD])
		DB.Update(Selector{UserID: message.OwnerID}, User{UserID: message.OwnerID, SystemNotification: false})
		return []linebot.Message{
			linebot.NewStickerMessage(sticker.PackageID, sticker.StickerID),
			linebot.NewTextMessage("ปิดการแจ้งเตือนจากระบบแล้ว"),
		}
	default:
		return []linebot.Message{linebot.NewTextMessage("คำสั่งผิดพลาดจำเป็นต้องป้อนคำสั่ง enabled หรือ disabled เท่านั้น. ตัวอย่าง /noti enabled")}
	}
}

/*
	Create template actions
	Max 3 templates
*/
func gotoInternalActions() []linebot.TemplateAction {
	return []linebot.TemplateAction{
		linebot.NewURITemplateAction("เปิด Portainer", "http://192.168.212.212:3002"),
		linebot.NewURITemplateAction("เปิด phpMyAdmin", "http://192.168.212.212:3000"),
		linebot.NewURITemplateAction("เปิด Jenkins", "http://192.168.212.212:8080"),
	}
}

/*
	Create template actions
	Max 3 templates
*/
func gotoExternalActions() []linebot.TemplateAction {
	return []linebot.TemplateAction{
		linebot.NewURITemplateAction("เปิด LINE Developer", "https://developers.line.me/en/"),
		linebot.NewURITemplateAction("เปิด Heroku", "https://dashboard.heroku.com/apps"),
		linebot.NewURITemplateAction("เปิด mLab", "https://mlab.com/home"),
	}
}

/*
	Create template message
 	Max 10 carousel columns
*/
func cmdMessage() *linebot.TemplateMessage {
	comingSoon := []linebot.TemplateAction{
		linebot.NewURITemplateAction("Coming soon", "http://www.example.com"),
		linebot.NewURITemplateAction("Coming soon", "http://www.example.com"),
		linebot.NewURITemplateAction("Coming soon", "http://www.example.com"),
	}

	columns := []*linebot.CarouselColumn{
		linebot.NewCarouselColumn("", "เซิร์ฟเวอร์ภายใน", "คุณจำเป็นต้องสามารถเข้าถึงเซิร์ฟเวอร์ภายในองกรณ์", gotoInternalActions()...),
		linebot.NewCarouselColumn("", "เซิร์ฟเวอร์ภายนอก", "การดำเนินการนี้ แค่นำทางไปเท่านั้น", gotoExternalActions()...),
		linebot.NewCarouselColumn("", "กำลังพัฒนา", "คำสั่งเพิ่มเติม...", comingSoon...),
	}
	ct := linebot.NewCarouselTemplate(columns...)
	message := linebot.NewTemplateMessage("กำลังแสดงคำสั่งทั้งหมด", ct)

	return message
}

/*
	Create template message
 	Max 10 carousel columns
*/
func gotoMessage() *linebot.TemplateMessage {
	columns := []*linebot.CarouselColumn{
		linebot.NewCarouselColumn("", "เซิร์ฟเวอร์ภายใน", "คุณจำเป็นต้องสามารถเข้าถึงเซิร์ฟเวอร์ภายในองกรณ์", gotoInternalActions()...),
		linebot.NewCarouselColumn("", "เซิร์ฟเวอร์ภายนอก", "การดำเนินการนี้ แค่นำทางไปเท่านั้น", gotoExternalActions()...),
	}
	ct := linebot.NewCarouselTemplate(columns...)
	message := linebot.NewTemplateMessage("กำลังแสดงคำสั่งทั้งหมด", ct)

	return message
}

/*
	Handle normal message and response
	@variable randomHelper have a possible 50 percent chance
*/
func normalMessage(textTrimed string, message Message) {
	stickerResponse := []linebot.Message{}
	textMessage := ""

	switch strings.ToLower(textTrimed) {
	case "hi", "hello", "สวัสดี", "หวัดดี":
		sticker := randomSticker(STICKER[STICKER_GROUP.GLAD])
		stickerResponse = append(stickerResponse, linebot.NewStickerMessage(sticker.PackageID, sticker.StickerID))
		textMessage = "สวัสดีครับ สบายดีมั๊ย"
	case "สบายดี":
		textMessage = "ก็ดีแล้ว"
	case "ไม่ค่อยสบาย":
		textMessage = "ต้องไปหาหมอที่โรงพยาบาลนะ"
	case "ไม่ไป":
		textMessage = "ต้องไป"
	case "ไม่":
		textMessage = "ไม่ อะไร"
	case "ไม่บอก":
		textMessage = "แล้วใครอยากรู้ล่ะ"
		sticker := randomSticker(STICKER[STICKER_GROUP.ANGRY])
		stickerResponse = append(stickerResponse, linebot.NewStickerMessage(sticker.PackageID, sticker.StickerID))
	case "โอเค":
		textMessage = "เยี่ยม"
	case "จะไป":
		textMessage = "เรื่องของ-ึง"
		sticker := randomSticker(STICKER[STICKER_GROUP.ANGRY])
		stickerResponse = append(stickerResponse, linebot.NewStickerMessage(sticker.PackageID, sticker.StickerID))
	case "เงียบ":
		textMessage = "ไม่อยากคุยด้วย"
	case "ลาก่อน", "ไปละ", "บาย":
		textMessage = "อย่าไปเลยนะ"
		sticker := randomSticker(STICKER[STICKER_GROUP.SAD])
		stickerResponse = append(stickerResponse, linebot.NewStickerMessage(sticker.PackageID, sticker.StickerID))
	default:
		if contains(textTrimed, []string{"ช่วย", "help"}) {
			textMessage = HELP_MESSAGE
		}
		if contains(textTrimed, []string{"อะไร", "ทำไร", "ทำอะไร", "ทำไม", "มีไร", "ว่าไง", "มีอะไร"}) {
			textMessage = "ไม่บอก"
		}
		if contains(textTrimed, []string{"มึง", "แม่ง", "เหี้ย", "กู", "ควย", "สัส", "พ่อง"}) {
			textMessage = "พูดไม่เพราะเลย ไม่คุยด้วยละ"
			sticker := randomSticker(STICKER[STICKER_GROUP.ANGRY])
			stickerResponse = append(stickerResponse, linebot.NewStickerMessage(sticker.PackageID, sticker.StickerID))
		}
		if contains(textTrimed, []string{"55", "ฮ่าๆ", "แหะๆ", "หึๆ", "หุๆ", "haha"}) {
			textMessage = "ฮ่าๆๆ ขำกลิ้ง"
		}
	}

	if len(textMessage) > 0 {
		response := append(stickerResponse, linebot.NewTextMessage(textMessage))
		logging(BOT.ReplyMessage(message.ReplyToken, response...).Do())
	}
}
