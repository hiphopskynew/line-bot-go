## Kafka Consumer Library ##

### Example for Kafka consumer ###

```go
package main

import (
	"bitbucket.org/dotography-code/luciano/kafka/consumer"
)

func main() {
	consumer := consumer.KafkaConsumer{
		Topics:      []string{"topic"},
		BrokerList: []string{"localhost:9092"},
		Partition:  0,
		GroupID: "test-group",
  }

  consumer.Handle(successFn, errorFn)
}

func successFn(msg consumer.Message) {
	...
}

func errorFn(str string) {
	...
}
```