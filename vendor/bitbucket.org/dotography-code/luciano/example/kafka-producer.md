## Kafka Producer ##

### Example for send message to the Kafka server ###

```go
package main

import (
	"bitbucket.org/dotography-code/luciano/kafka/producer"
)

func main() {
	producer := producer.KafkaProducer{
		Topic:      "topic",
		MaxRetry:   5,
		BrokerList: []string{"localhost:9092"},
	}

	producer.Send("key", "message") // can using interface type for message
}
```