package consumer

import (
	"os"
	"os/signal"

	cluster "github.com/bsm/sarama-cluster"
)

type KafkaConsumer struct {
	BrokerList []string
	Topics     []string
	Partition  int32
	GroupID    string
}

type Message struct {
	Key, Value []byte
}

type HandleSuccess func(Message)
type HandleError func(string)

func (kc KafkaConsumer) Handle(hs HandleSuccess, he HandleError) {
	config := cluster.NewConfig()
	config.Consumer.Return.Errors = true

	// init consumer
	consumer, err := cluster.NewConsumer(kc.BrokerList, kc.GroupID, kc.Topics, config)

	if err != nil {
		panic(err)
	}
	defer consumer.Close()

	// trap SIGINT to trigger a shutdown.
	signals := make(chan os.Signal, 1)
	signal.Notify(signals, os.Interrupt)
	doneCh := make(chan struct{})

	// consume messages, watch signals
	go func() {
		for {
			select {
			case err := <-consumer.Errors():
				he(err.Error())
			case msg, ok := <-consumer.Messages():
				if ok {
					consumer.MarkOffset(msg, "") // mark message as processed
					hs(Message{
						Key:   msg.Key,
						Value: msg.Value,
					})
				}
			case <-signals:
				doneCh <- struct{}{}
				return
			}
		}
	}()
	<-doneCh
}
