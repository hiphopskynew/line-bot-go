package producer

import (
	"encoding/json"

	"github.com/Shopify/sarama"
)

type KafkaResult struct {
	Topic     string      `json:"topic"`
	Key       string      `json:"key"`
	Message   interface{} `json:"message"`
	Partition int32       `json:"partition,omitempty"`
	Offset    int64       `json:"offset,omitempty"`
	Error     error       `json:"error,omitempty"`
}

type KafkaProducer struct {
	BrokerList []string
	Topic      string
	MaxRetry   int
}

func (kafka KafkaProducer) Send(key string, message interface{}) KafkaResult {
	result := KafkaResult{
		Topic:   kafka.Topic,
		Key:     key,
		Message: message,
		Error:   nil,
	}
	config := sarama.NewConfig()
	config.Producer.RequiredAcks = sarama.WaitForAll
	config.Producer.Retry.Max = kafka.MaxRetry
	config.Producer.Return.Successes = true

	msgArrBytes, err := json.Marshal(message)
	if err != nil {
		result.Error = err
		return result
	}

	producer, err := sarama.NewSyncProducer(kafka.BrokerList, config)
	if err != nil {
		result.Error = err
		return result
	}

	defer func() {
		if err := producer.Close(); err != nil {
			panic(err)
		}
	}()

	partition, offset, err := producer.SendMessage(&sarama.ProducerMessage{
		Topic: kafka.Topic,
		Key:   sarama.StringEncoder(key),
		Value: sarama.StringEncoder(string(msgArrBytes)),
	})

	if err != nil {
		result.Error = err
		return result
	}

	result.Partition = partition
	result.Offset = offset
	result.Error = err

	return result
}
