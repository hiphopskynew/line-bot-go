package stdout

import (
	"fmt"
	"strings"
)

func log(level string, itf ...interface{}) {
	fmt.Println("["+strings.ToUpper(level)+"]", itf)
}

func Trace(itf ...interface{}) {
	log("trace", itf...)
}

func Info(itf ...interface{}) {
	log("info", itf...)
}

func Debug(itf ...interface{}) {
	log("debug", itf...)
}

func Warning(itf ...interface{}) {
	log("warning", itf...)
}

func Error(itf ...interface{}) {
	log("error", itf...)
}
