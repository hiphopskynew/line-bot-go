package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"strings"
	"time"

	"bitbucket.org/dotography-code/luciano/logger/stdout"

	"github.com/line/line-bot-sdk-go/linebot"
)

type PingObject struct {
	IPAddress  string `json:"ip_address"`
	Port       string `json:"port"`
	PingResult string `json:"ping_result"`
}

type NotifyRequest struct {
	Type string                `json:"type"`
	Data map[string]PingObject `json:"data"`
}

var lastNR *NotifyRequest = &NotifyRequest{}
var reachable map[string]bool = map[string]bool{
	"before":  false,
	"current": false,
}
var lastReceivePing int64 = 0

func buildNotify(nr NotifyRequest) map[string]string {
	m := make(map[string]string)
	lData := lastNR.Data

	for k, v := range nr.Data {
		if lData[k].PingResult == v.PingResult {
			continue
		}
		if strings.Contains(v.PingResult, "open") {
			m[k] = "กลับมาใช้งานได้แล้ว"
		} else {
			m[k] = "ไม่สามารถใช้งานได้"
		}
	}

	return m
}

/*
	Notify handle for LINE push
	JSON format follow this
	{
		"type": "ping",
		"data": {
			":server_name": {
				"ip_address": ":ip_address",
				"port": ":port",
				"ping_result": ":ping_result"
			},
			":server_name": {
				"ip_address": ":ip_address",
				"port": ":port",
				"ping_result": ":ping_result"
			}
			...
		}
	}
*/
func notify(w http.ResponseWriter, r *http.Request) {
	defer r.Body.Close()
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		stdout.Debug("Notify function")
		stdout.Error(err.Error())
	}

	nr := NotifyRequest{}
	json.Unmarshal(body, &nr)

	m := buildNotify(nr)
	switch nr.Type {
	case "ping":
		lastReceivePing = now()
		if len(m) == 0 {
			return
		}
		list := []string{"[ระบบ]"}
		for k, v := range m {
			list = append(list, fmt.Sprintf("%s, %s", k, v))
		}
		msg := strings.Join(list, "\n")
		lastNR = &nr
		logging(BOT.Multicast(getUserAllowNotiIDs(), linebot.NewTextMessage(msg)).Do())
	}
}

func now() int64 {
	return time.Now().UnixNano() / int64(time.Millisecond)
}

// Start watching notification
func startWatchNotify() {
	for {
		checkReachableServer()
		time.Sleep(time.Second * 5)
	}
}

func checkReachableServer() {
	// If PING start consumer `lastReceivePing` is not equal 0
	if lastReceivePing == 0 {
		return
	}
	reachable["current"] = (now() - lastReceivePing) <= 2000*30
	// Returned if message sent to users
	if reachable["before"] == reachable["current"] {
		return
	}

	// Build message to users
	msg := ""
	if reachable["before"] && !reachable["current"] {
		msg = "มีปัญหาไม่สามารถเข้าถึงเครื่องเซิร์ฟเวอร์ได้"
	}
	if !reachable["before"] && reachable["current"] {
		msg = "สามารถเข้าถึงเครื่องเซิร์ฟเวอร์ได้แล้ว"
	}
	reachable["before"] = reachable["current"]
	logging(BOT.Multicast(getUserAllowNotiIDs(), linebot.NewTextMessage(fmt.Sprintf("[ระบบ]\n%s", msg))).Do())
}
