package main

import (
	"log"
	"net/http"
	"os"

	"github.com/line/line-bot-sdk-go/linebot"
)

func main() {
	bot, err := linebot.New(CHANNEL_SECRET, CHANNEL_TOKEN)
	if err != nil {
		log.Fatal(err)
	}
	onBoot(bot)

	http.HandleFunc("/callback", callback)
	http.HandleFunc("/notify", notify)
	http.HandleFunc("/pushmessage", pushMessage)
	http.HandleFunc("/voicecommand", voiceCommandHandle)
	http.HandleFunc("/command", func(w http.ResponseWriter, r *http.Request) {
		w.Write([]byte(cmdPool.getAndClear()))
	})
	go startWatchNotify()

	if err := http.ListenAndServe(":"+os.Getenv("PORT"), nil); err != nil {
		log.Fatal(err)
	}
}
