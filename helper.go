package main

import (
	"math/rand"
	"strings"
)

func randomSticker(slice []Sticker) Sticker {
	rand.Shuffle(len(slice), func(i, j int) {
		slice[i], slice[j] = slice[j], slice[i]
	})
	return slice[0]
}

func randomSliceOfString(slice []string) string {
	rand.Shuffle(len(slice), func(i, j int) {
		slice[i], slice[j] = slice[j], slice[i]
	})
	return slice[0]
}

func removeItemInSlice(slice []string, item string) []string {
	result := []string{}
	for _, value := range slice {
		if value == item {
			continue
		}
		result = append(result, value)
	}

	return result
}

func contains(str string, subStrs []string) bool {
	for _, v := range subStrs {
		if strings.Contains(str, v) {
			return true
		}
	}
	return false
}

func removeEmptyInSlice(slice []string) []string {
	msgs := []string{}
	for _, msg := range slice {
		if len(strings.TrimSpace(msg)) > 0 {
			msgs = append(msgs, msg)
		}
	}
	return msgs
}
