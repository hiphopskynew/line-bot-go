package main

import (
	"github.com/line/line-bot-sdk-go/linebot"
)

func addUsers(events []*linebot.Event) {
	for _, event := range events {
		selector := Selector{UserID: event.Source.UserID}
		if DB.UserExists(selector) {
			continue
		}
		user := User{UserID: event.Source.UserID, SystemNotification: true}
		DB.Insert(user)
	}
}

func getUserAllIDs() []string {
	users, err := DB.FindAll()
	if err != nil {
		panic(err)
	}

	ids := []string{}
	for _, user := range users {
		ids = append(ids, user.UserID)
	}
	return ids
}

func getUserAllowNotiIDs() []string {
	users, err := DB.FindAll()
	if err != nil {
		panic(err)
	}

	ids := []string{}
	for _, user := range users {
		if user.SystemNotification {
			ids = append(ids, user.UserID)
		}
	}
	return ids
}
