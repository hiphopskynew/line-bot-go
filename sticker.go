package main

type Sticker struct {
	PackageID string
	StickerID string
}

type StickerGroup struct {
	GLAD     string
	SAD      string
	ANGRY    string
	SURPRISE string
	ALL      string
}

func NewStickerGroup() *StickerGroup {
	return &StickerGroup{
		GLAD:     "glad",
		SAD:      "sad",
		ANGRY:    "angry",
		SURPRISE: "surprise",
		ALL:      "all",
	}
}

var (
	STICKER_GROUP *StickerGroup        = NewStickerGroup()
	STICKER       map[string][]Sticker = map[string][]Sticker{
		STICKER_GROUP.GLAD: []Sticker{
			Sticker{PackageID: "1", StickerID: "2"},
			Sticker{PackageID: "1", StickerID: "5"},
			Sticker{PackageID: "1", StickerID: "10"},
			Sticker{PackageID: "1", StickerID: "11"},
			Sticker{PackageID: "1", StickerID: "13"},
			Sticker{PackageID: "1", StickerID: "14"},
			Sticker{PackageID: "1", StickerID: "103"},
			Sticker{PackageID: "1", StickerID: "106"},
			Sticker{PackageID: "1", StickerID: "114"},
			Sticker{PackageID: "1", StickerID: "116"},
			Sticker{PackageID: "1", StickerID: "119"},
			Sticker{PackageID: "1", StickerID: "120"},
			Sticker{PackageID: "1", StickerID: "125"},
			Sticker{PackageID: "1", StickerID: "130"},
			Sticker{PackageID: "1", StickerID: "132"},
			Sticker{PackageID: "1", StickerID: "134"},
			Sticker{PackageID: "1", StickerID: "136"},
			Sticker{PackageID: "1", StickerID: "137"},
			Sticker{PackageID: "1", StickerID: "138"},
			Sticker{PackageID: "1", StickerID: "139"},
			Sticker{PackageID: "1", StickerID: "402"},
			Sticker{PackageID: "1", StickerID: "407"},
			Sticker{PackageID: "1", StickerID: "410"},
			Sticker{PackageID: "1", StickerID: "412"},
			Sticker{PackageID: "1", StickerID: "413"},
			Sticker{PackageID: "1", StickerID: "414"},
			Sticker{PackageID: "1", StickerID: "428"},
		},
		STICKER_GROUP.SAD: []Sticker{
			Sticker{PackageID: "1", StickerID: "9"},
			Sticker{PackageID: "1", StickerID: "16"},
			Sticker{PackageID: "1", StickerID: "131"},
		},
		STICKER_GROUP.ANGRY: []Sticker{
			Sticker{PackageID: "1", StickerID: "6"},
			Sticker{PackageID: "1", StickerID: "7"},
			Sticker{PackageID: "1", StickerID: "8"},
			Sticker{PackageID: "1", StickerID: "17"},
			Sticker{PackageID: "1", StickerID: "101"},
			Sticker{PackageID: "1", StickerID: "102"},
			Sticker{PackageID: "1", StickerID: "105"},
			Sticker{PackageID: "1", StickerID: "109"},
			Sticker{PackageID: "1", StickerID: "113"},
			Sticker{PackageID: "1", StickerID: "118"},
			Sticker{PackageID: "1", StickerID: "127"},
			Sticker{PackageID: "1", StickerID: "133"},
			Sticker{PackageID: "1", StickerID: "415"},
			Sticker{PackageID: "1", StickerID: "420"},
			Sticker{PackageID: "1", StickerID: "421"},
		},
		STICKER_GROUP.SURPRISE: []Sticker{
			Sticker{PackageID: "1", StickerID: "14"},
			Sticker{PackageID: "1", StickerID: "106"},
			Sticker{PackageID: "1", StickerID: "125"},
			Sticker{PackageID: "1", StickerID: "134"},
			Sticker{PackageID: "1", StickerID: "137"},
		},
	}
	STICKERS []Sticker = func() []Sticker {
		stickers := []Sticker{}
		for _, v := range STICKER {
			stickers = append(stickers, v...)
		}
		return stickers
	}()
)
