package main

import (
	"os"

	"github.com/line/line-bot-sdk-go/linebot"
	mgo "gopkg.in/mgo.v2"
)

var (
	FOX_URL                 = "https://upload.wikimedia.org/wikipedia/commons/e/ec/Stony_Brook_Athletics_Primary_Logo.png"
	BOT                     *linebot.Client
	DB                      MongoDB
	LINEBOT_USER_ID         = os.Getenv("LINEBOT_USER_ID")
	CHANNEL_SECRET          = os.Getenv("CHANNEL_SECRET")
	CHANNEL_TOKEN           = os.Getenv("CHANNEL_TOKEN")
	MONGO_URL               = os.Getenv("MONGO_URL")
	DIALOGFLOW_CALLBACK_URL = os.Getenv("DIALOGFLOW_CALLBACK_URL")
	HELP_MESSAGE            = `[คำสั่ง]
/all <ข้อความ> (ส่งข้อความถึงเพื่อนทุกคนของไลน์บอท)

/me <ข้อความ> (ไลน์บอทจะส่งข้อความเดียวกับที่คุณส่งไปกลับคืน)

/goto	(แสดงทางลัดไปยังหน้าจัดการระบบทั้งภายใน และภายนอก)

/docker (สามารถใช้คำสั่งเสมือน bash ตัวอย่าง '/docker restart ems')

/noti <enabled หรือ disabled> (ตั้งค่าเปิดหรือปิดการแจ้งเตือนอัตโนมัติจากระบบ)

/help (แสดงข้อความช่วยเหลือ)`
)

func onBoot(bot *linebot.Client) {
	BOT = bot
	DB = dbInitSession()
	NotiStartServer()
	NewCmdPool()
}

func dbInitSession() MongoDB {
	session, err := mgo.Dial(MONGO_URL)
	session.SetPoolLimit(100)
	if err != nil {
		panic(err)
	}
	return MongoDB{session: session}
}
