package main

import "github.com/line/line-bot-sdk-go/linebot"

func NotiStartServer() {
	BOT.Multicast(
		getUserAllowNotiIDs(),
		linebot.NewTextMessage(
			`[ระบบ]
ต้องการความช่วยเหลือพิมพ์ "/help"
หากต้องการปิดการแจ้งเตือนจากระบบพิมพ์ "/noti disabled"`),
	).Do()
}
